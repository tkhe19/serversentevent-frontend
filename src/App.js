import logo from "./logo.svg";
import "./App.css";
import React, { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [name, setName] = useState("Waiting for data ...");

  useEffect(() => {
    var source = new EventSource("http://127.0.0.1:5000/events");
    source.addEventListener(
      "customer",
      function(event) {
        var data = JSON.parse(event.data);
        console.log(data[0].name);
        setName(data[0].name);
      },
      false
    );
    source.addEventListener(
      "error",
      function(event) {
        console.log(event);
      },
      false
    );
  });

  return (
    <div
      style={{
        fontSize: "2rem",
        padding: "2rem 0rem 0rem 2rem",
        color: "black",
      }}
    >
      Name: {name}
    </div>
  );
}

export default App;
